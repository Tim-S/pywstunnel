# PyWsTunnel

## Prerequisites

```bash
pip install -r requirements.txt
```

## Examples

Start Server

```bash
python WebSocketTunel.py --server 0.0.0.0:8000
```

Client

```bash
python WebSocketTunel.py ws://localhost:8000 -L 127.0.0.1:1337:127.0.0.1:222
```
