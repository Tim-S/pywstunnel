import argparse
import aiohttp
import asyncio
import re
import sys

from aiohttp import WSMsgType, web
from asyncio import StreamReader, StreamWriter
from typing import Callable, List, Optional

def get_host_pair(value):
    return re.search(r'^(.*?)(?::(\d+))?$', value).groups()

class WebSocketTunnel:
    def __init__(self, ws_server_url: str, timeout=30) -> None:
        self.ws_server_url = ws_server_url
        self.timeout = timeout

    async def run_tunnel_client(self, local_host: str, port: int, remote_port: int, remote_host = '127.0.0.1'):
        async def _local_port_handler(reader: StreamReader, writer: StreamWriter):
            # connect to websocket-server
            async with aiohttp.ClientSession(trust_env=True) as websocket_client:
                websocket_connection = await websocket_client.ws_connect(self.ws_server_url)
                close_event = asyncio.Event()
                # inform the server that he needs to establish a port-forward to (remote_dest_host: remote_dest_port) for us
                await websocket_connection.send_json({ 
                        'remote_host': remote_host, 
                        'remote_port': remote_port
                    })
                try:
                    await asyncio.gather(
                        forward_stream_to_websocket(reader, websocket_connection, close_event),
                        forward_websocket_to_stream(websocket_connection, writer, close_event)
                    )
                finally:
                    await websocket_connection.close()
                    writer.close()

        server = await asyncio.start_server(
            _local_port_handler, local_host, port
        )
        return server

    def create_tunnel_server_application(self, path='/{tail:.*}') -> web.Application:

        app = web.Application()
        # sockets = web.AppKey("sockets", List[web.WebSocketResponse]) # will be supported in aiohttp 4.0

        l: List[web.WebSocketResponse] = []
        app["sockets"] = l

        async def websocket_handler(request):
            print('Websocket connection starting')
            websocket_connection = aiohttp.web.WebSocketResponse()
            await websocket_connection.prepare(request)
            print('Websocket connection ready')
            
            close_event = asyncio.Event()

            # wait for local_forwarding_request from client
            local_forwarding_request = (await websocket_connection.receive()).json()
            remote_host, remote_port = (
                str(local_forwarding_request['remote_host']),
                int(local_forwarding_request['remote_port'])
            )
            peername = request.transport.get_extra_info('peername')
            print(f"Localforward from {peername} --> {(remote_host, remote_port)}", )
            
            # connect to tcp
            reader, writer = await asyncio.open_connection(remote_host, remote_port)

            def _text_msg_callback(msg, _ws):
                local_forwarding_request = msg.json()
                print(local_forwarding_request)

            # forward websocket binary messages to tcp socket
            await asyncio.gather(
                forward_stream_to_websocket(reader, websocket_connection, close_event),
                forward_websocket_to_stream(websocket_connection, writer, close_event, text_msg_callback=_text_msg_callback)
            )

            await websocket_connection.close()
            print('Websocket connection closed')
            return websocket_connection

        async def on_shutdown(app: web.Application) -> None:
            for websocket_connection in app["sockets"]:
                await websocket_connection.close()
        
        app.router.add_get(path, websocket_handler)
        app.on_shutdown.append(on_shutdown)
        return app # webserver can be started with web.run_app(app)


async def forward_stream_to_websocket(reader: StreamReader, websocket, event: asyncio.Event):
    while not event.is_set():
        try:
            data = await asyncio.wait_for(reader.read(1024), 1)
        except asyncio.TimeoutError:
            continue

        if data == b'':  # when it closed
            event.set()
            break

        await websocket.send_bytes(data)


async def forward_websocket_to_stream(websocket, writer: StreamWriter, event: asyncio.Event, text_msg_callback: Optional[Callable[[aiohttp.WSMessage, aiohttp.ClientWebSocketResponse], None]]=None):
    while not event.is_set():
        try:
            # data = await asyncio.wait_for(websocket.receive_bytes(), 1)
            msg = await asyncio.wait_for(websocket.receive(), 1)
            if msg.type ==  WSMsgType.BINARY:
                data = msg.data
            elif msg.type ==  WSMsgType.TEXT :
                if text_msg_callback:
                    text_msg_callback(msg, websocket)
                continue
            elif msg.type == WSMsgType.CLOSE:
                event.set()
                break
            else: # WSMsgType.PING or WSMsgType.PONG 
                continue

        except asyncio.TimeoutError:
            continue

        if data == b'':  # when it closed
            event.set()
            break

        writer.write(data)
        await writer.drain()

def main():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("address", nargs='?', const=1, type=str, default="0.0.0.0:8000")
    parser.add_argument('--server', action='store_true')
    parser.add_argument("-L", "--local-forwarding",   dest='local_forwarding', help="""Local Forwarding.
        Example: '-L 127.0.0.1:1337:127.0.0.1:22' listens on port 1337 locally and 
        forwards each connection to port 22 of a server listenening on 127.0.1 from the websocketserver.""", type=str)
    args = parser.parse_args()
    
    host, port = get_host_pair(args.address)

    if(args.server):
        app = WebSocketTunnel(ws_server_url=args.address).create_tunnel_server_application(
            path ='/{tail:.*}' # any path (.*)
        )
        web.run_app(app, port=port, host=host)
        return

    # running as client (default)

    local_forwarding = args.local_forwarding
    if(local_forwarding):
        def local_forward_arg_to_tokens(input: str):
            return [ match.group(0) for match in 
                re.finditer(r'(([a-zA-Z0-9\.\-]+)|(\[.*\]))', input)]

        _forwarding_args = local_forward_arg_to_tokens(local_forwarding)
        local_src_ip = _forwarding_args[-4] if len(_forwarding_args) == 4 else '127.0.0.1'
        local_port = _forwarding_args[-3]
        remote_dest_host = _forwarding_args[-2]
        remote_dest_port = _forwarding_args[-1]
        # print(f"tunnel {local_src_ip}:{local_port}--(websocket {args.address})-->{remote_dest_host}:{remote_dest_port}")

        async def tunnel_client():
            server = await WebSocketTunnel(ws_server_url=args.address).run_tunnel_client(
                local_host=local_src_ip, port=local_port, remote_host=remote_dest_host, remote_port=remote_dest_port)
            async with server:
                await server.serve_forever()
        asyncio.run(tunnel_client())
        return

    parser.print_help()
    print("If running as client make sure to pass at least one Localforwarding.", file=sys.stderr)

if __name__ == "__main__":
    main()